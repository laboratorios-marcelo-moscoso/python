import pyodbc

# Establish a connection to the PostgreSQL database
connection = pyodbc.connect(
    "Driver={PostgreSQL ODBC Driver(UNICODE)};"
    "Server=host_name;"
    "Port=port_number;"
    "Database=database_name;"
    "Uid=username;"
    "Pwd=password;"
)

# Create a cursor object to interact with the database
cursor = connection.cursor()

# Execute a SQL query
cursor.execute("SELECT * FROM employees")

# Fetch and print the result set
for row in cursor:
    print(row)

# Close the cursor and the connection
cursor.close()
connection.close()
