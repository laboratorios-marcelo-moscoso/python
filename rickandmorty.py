
import requests

def invoke_api():
    url = "https://rickandmortyapi.com/api/character"  # Replace with your API endpoint

    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for 4xx or 5xx status codes
        data = response.json()  # Convert the response to JSON

        # Process the data
        print("Response:", data)

    except requests.exceptions.RequestException as e:
        # Handle any errors that occurred during the request
        print("Error:", e)

invoke_api()
