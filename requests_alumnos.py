import requests
import json


url = "https://codigoinfo.xyz/api/alumnos"


data = {
    "nombre_completo": "Julio Vargas",
    "email": "julio@test.com",
    "imagen":""
}

json_data = json.dumps(data)

headers = {'Content-Type': 'application/json'}

response = requests.post(url, data=json_data,headers=headers)

if response.status_code == 200:
    print(response.content)
else:
    print("Error:", response.status_code)
