

# 1. Integer to String:
num = 42
num_str = str(num)
print(type(num_str))  # Output: <class 'str'>

# 2. String to Integer:
num_str = "42"
num = int(num_str)
print(type(num))  # Output: <class 'int'>

# 3. String to Float:
float_str = "3.14"
float_num = float(float_str)
print(type(float_num))  # Output: <class 'float'>

# 4. Float to Integer:
float_num = 3.14
int_num = int(float_num)
print(type(int_num))  # Output: <class 'int'>

# 5. Integer to Float:
int_num = 42
float_num = float(int_num)
print(type(float_num))  # Output: <class 'float'>

# 6. String to Boolean:
bool_str = "True"
bool_val = bool(bool_str)
print(type(bool_val))  # Output: <class 'bool'>
print(bool_val)  # Output: True

# 7. Integer to Boolean:
num = 0
bool_val = bool(num)
print(type(bool_val))  # Output: <class 'bool'>
print(bool_val)  # Output: False

