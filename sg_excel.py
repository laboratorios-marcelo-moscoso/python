import PySimpleGUI as sg
import pandas as pd

sg.theme('DarkTeal9')

EXCEL_FILE = 'alumnos.xlsx'
#df = pd.read_excel(EXCEL_FILE)

layout = [
    [sg.Text('Registro de Alumnos',font=("Verdana", 20))],
    [sg.Text("Nombres",font=("Verdana", 20),size=(15,1)),sg.InputText(key='-NOMBRES-',font=("Verdana", 20))],
    [sg.Text("Email",font=("Verdana", 20),size=(15,1)),sg.InputText(key='-EMAIL-',font=("Verdana", 20))],
    [sg.Button('Registrar',font=("Verdana", 20)), sg.Button('Cancelar',font=("Verdana", 20))],
    [sg.Text(key='-MENSAJE-',font=("Verdana", 20))]
]

window = sg.Window('Aplicacion de Saludo', layout)

while True:
      event, values = window.read()
      if event == sg.WIN_CLOSED or event == 'Cancelar':
         break
      if event == 'Registrar':
        window['-MENSAJE-'].update(values['-NOMBRES-'])
        print(event,values)
        data = {
            'nombre': [values['-NOMBRES-']],
            'email': [values['-EMAIL-']]
        }
        df = pd.DataFrame(data)
        df.to_excel(EXCEL_FILE,index=False)
        sg.popup('Datos insertados',font=("Verdana", 20))

window.close()

