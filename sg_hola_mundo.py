import PySimpleGUI as sg

sg.theme('SystemDefault')

# disenio de la pantalla
layout = [
    [sg.Text('Hola mundo Python',font=('Impact',40))]
]

window = sg.Window('Applicacion de saludos',layout)

while True:
    event,value = window.read()
    if event == sg.WIN_CLOSED:
        print('estamos cerrando la ventana')
        break

window.close()