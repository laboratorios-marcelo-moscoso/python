import PySimpleGUI as sg

sg.theme('BlueMono')
# disenio de la pantalla
layout = [
    [sg.Text('Introduzca su nombre:',font=('Verdana',20))],
    [sg.InputText(key='-NOMBRE-',font=('Verdana',20))],
    [sg.Button('Aceptar',font=('Verdana',20)),sg.Button('Cancelar',font=('Verdana',20)) ],
    [sg.Text(key='-MENSAJE-',font=('Verdana',20))]
]

window = sg.Window('Applicacion de saludos',layout)

while True:
    event,values = window.read()
    if event == sg.WIN_CLOSED or event == 'Cancelar':
        print('estamos cerrando la ventana')
        break
    if event == 'Aceptar':
        nombre = values['-NOMBRE-']
        window['-MENSAJE-'].update(nombre)

window.close()