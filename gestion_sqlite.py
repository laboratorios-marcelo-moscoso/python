
import sqlite3

try:
    # Conneccion a SQLite
    conn = sqlite3.connect('data/alumnos.db')

    # Create a cursor
    cursor = conn.cursor()

    # Create Table

    cursor.execute('''CREATE TABLE IF NOT EXISTS alumnos
                    ( id integer primary key autoincrement,
                    nombre_completo text,
                    email text )  ''')

    # Insertar registros a la tabla

    #cursor.execute(''' INSERT INTO alumnos (nombre_completo,email)
    #                    VALUES ('Ana Gomez','ana.gomez@text.com')
    #''')
    #conn.commit()

    # input param
    
    nombre_completo = input('Introduzca su nombre completo: ')
    email = input('Introduzca su email: ')
    edad = int(input("edad"))

    cursor.execute('''INSERT INTO alumnos (nombre_completo,email)
                        VALUES (?,?) ''',(nombre_completo,email))
    conn.commit()

    # query table

    cursor.execute("SELECT * FROM alumnos ")

    rows = cursor.fetchall()

    for row in rows:
        print(f"ID:{row[0]}, Nombre completo:{row[1]}, email:{row[2]} ")

except Exception as e:
    print(f"Error general al abrir la base de datos {e} ")
finally:
    if conn:
        conn.close()