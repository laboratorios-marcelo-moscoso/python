import PySimpleGUI as sg
import sqlite3

sg.theme('DarkTeal9')

conn = sqlite3.connect('data/alumnos.db')

conn.execute('''CREATE TABLE IF NOT EXISTS alumnos
                    ( id integer primary key autoincrement,
                    nombre_completo text,
                    email text )  ''')

def get_alumnos():
    cursor =conn.execute('SELECT * FROM alumnos')
    return cursor.fetchall()

def registrar(nombre_completo,email):
    conn.execute('INSERT INTO alumnos (nombre_completo, email) VALUES (?,?)',(nombre_completo,email))
    conn.commit()


sg.theme('BlueMono')
# disenio de la pantalla
layout = [
    [
        sg.Column(
            [
                [sg.Text('Nombre completo:',font=('Verdana',20))],
                [sg.InputText(key='-NOMBRE_COMPLETO-',font=('Verdana',20))],
                [sg.Text('Email:',font=('Verdana',20))],
                [sg.InputText(key='-EMAIL-',font=('Verdana',20))],
                [
                    sg.Button('Registrar DB',font=('Verdana',20)),
                    sg.Button('Cancelar',font=('Verdana',20)) 
                ],
                [sg.Text(key='-MENSAJE-',font=('Verdana',20))]
            ]
        ),
        sg.VSeperator(),
        sg.Column(
            [
                [sg.Text('Lista de Alumnos',font=('Verdana',20))],
                [sg.Table(values=get_alumnos(),headings =['ID','NOMBRE','EMAIL'],key='-LISTA-',enable_events = True,font=("Verdana",20))],
                [sg.Button('Exportar EXCEL',font=('Verdana',20))]
            ]
        )
    ]
]


window = sg.Window('Registro alumnos',layout)

while True:
    event,values = window.read()

    if event == sg.WIN_CLOSED or event == 'Cancelar':
        print('estamos cerrando la ventana')
        break

    if event == '-LISTA-':
        print(values['-LISTA-'])
        print(values['-LISTA-'][0])

    if event == 'Registrar DB':
        nombre_completo = values['-NOMBRE_COMPLETO-']
        email = values['-EMAIL-']
        registrar(nombre_completo,email)
        window['-MENSAJE-'].update('Se registro con exito')
        window['-NOMBRE_COMPLETO-'].update('')
        window['-EMAIL-'].update('')


# Cerrar la conexion
conn.close()
# Cerrar App
window.close()