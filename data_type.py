

# 1. Entero (`int`):

edad = 25
cantidad = -10

# 2. Flotante (`float`):

pi = 3.14
temperatura = -2.5

# 3. Complejo (`complex`):

z = 2 + 3j

# 4. Cadena de caracteres (`str`):

nombre = "Juan Pérez"
mensaje = 'Hola, Mundo!'

# 5. Booleano (`bool`):

es_verdadero = True
es_falso = False

# 6. Lista (`list`):

numeros = [1, 2, 3, 4, 5]
frutas = ['manzana', 'banana', 'cereza']

# 7. Tupla (`tuple`):

coordenadas = (10, 20)
nombres = ('Alice', 'Bob', 'Charlie')

# 8. Conjunto (`set`):

numeros_unicos = {1, 2, 3, 4, 5}
vocales = {'a', 'e', 'i', 'o', 'u'}

# 9. Diccionario (`dict`):

estudiante = {'nombre': 'Juan', 'edad': 20, 'carrera': 'Informática'}

# 10. Nulo (`None`):
 
resultado = None
