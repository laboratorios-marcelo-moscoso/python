from flask import Flask,render_template,request,redirect,url_for
import sqlite3


def get_alumnos():
    conn = sqlite3.connect('data/alumnos.db')
    cursor =conn.execute('SELECT * FROM alumnos')
    return cursor.fetchall()

def registrar(nombre_completo,email):
    conn = sqlite3.connect('data/alumnos.db')
    conn.execute('INSERT INTO alumnos (nombre_completo, email) VALUES (?,?)',(nombre_completo,email))
    conn.commit()
    conn.close()

def delete_alumno(id):
    conn = sqlite3.connect('data/alumnos.db')
    conn.execute('DELETE FROM alumnos WHERE id = ? VALUES (?)',(id))
    conn.commit()
    conn.close()

app = Flask(__name__)

#routers
@app.route('/',methods=['GET'])
def index():
    alumnos = get_alumnos()
    return render_template('index.html',rows=alumnos)

@app.route('/add-alumno',methods=['POST'])
def add_alumno():
    nombre_completo = request.form['nombre_completo']
    email = request.form['email']
    print(nombre_completo,email)
    registrar(nombre_completo,email)
    return redirect(url_for('index'))

@app.route('/delete-alumno/<int:alumno_id>',methods=['GET'])
def delete_alumno(alumno_id):
    print(alumno_id)
    return redirect(url_for('index'))


#server
if __name__ == '__main__':
    app.run(debug=True,port=3000)