import requests
import PySimpleGUI as sg

# Make API call and get response
response = requests.get('https://rickandmortyapi.com/api/character')
data = response.json()

# Prepare list items with images
list_items = []
for character in data['results']:
    print(character['name'])
    name = character['name']
    list_items.append([name])
    #image = sg.Image(character['image'], size=(100, 100))
    #name = sg.Text(character['name'])
    #species = sg.Text(character['species'])
    #status = sg.Text(character['status'])
    #list_items.append(23[image, name, species, status])

# Define list layout
list_layout = [
    [sg.Listbox(values=list_items, pad=(0, 10),font=('Verdana',10),expand_y=True)]
]

# Create the window
window = sg.Window('Rick and Morty Characters',list_layout,size=(400,600))

# Event loop
while True:
    event, values = window.read()
    if event == sg.WINDOW_CLOSED:
        break

# Close the window
window.close()
