from flask import Flask, render_template,request,redirect,url_for

app = Flask(__name__)

#routers 
@app.route('/',methods=['GET']) 
def index():
    return render_template('index.html')


@app.route('/add_curso',methods=['POST'])
def add_curso():
    return redirect(url_for('index'))

# server
if __name__ == '__main__':
    app.run(debug=True,port=3000)
