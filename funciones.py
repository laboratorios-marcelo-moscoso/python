# Funcion lista con parametros

def getAlumnos(lista):
    for alumno in lista:
        print(alumno)

print("linea adicional")

alumnos = ["ana","jorge","maria","andres"]
# getAlumnos(alumnos)


## funcion any() verifica si al menos uno de los elementos de una secuencia cumple con una condicion. 
def buscar_letras(linea, letras):
    return any(letra in linea for letra in letras)

texto = 'Python también permite crear tipos de datos personalizados utilizando clases y objetos 23456709-19062023 '
texto_buscadas = ['23456709']

resultado = buscar_letras(texto,texto_buscadas)

print(resultado)

# Función lambda que suma dos números
add = lambda x, y: x + y
result = add(5, 3)
print(result)  # Output: 8

# Función lambda que devuelve el cuadrado de un número
square = lambda x: x**2
result = square(4)
print(result)  # Output: 16

# Función lambda que verifica si un número es par
is_even = lambda x: x % 2 == 0
result = is_even(7)
print(result)  # Output: False


