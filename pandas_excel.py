import pandas as pd

data = { 
        "nombres" : ['ana','jorge','maria'],
        "email" : ['ana@tex.com','jorge@test.com','maria@test.com']
}

df = pd.DataFrame(data)

excel_file_path = 'alumnos.xlsx'

df.to_excel(excel_file_path,index=False)