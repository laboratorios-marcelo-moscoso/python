import pyodbc

# Establish a connection to the SQLite3
connection = pyodbc.connect("Driver=SQLite3;Database=data/alumnos.db")

# Create a cursor object to interact with the database
cursor = connection.cursor()

# Execute a SQL query
cursor.execute("SELECT * FROM alumnos")

# Fetch and print the result set
for row in cursor:
    print(row)

# Close the cursor and the connection
cursor.close()
connection.close()