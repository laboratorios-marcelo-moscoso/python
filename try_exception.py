import sqlite3
import requests

## try for file
file_path = 'datos2.txt'

try:

    with open(file_path,'r') as file:
        print(file)

except FileNotFoundError:
    print(f"Archivo {file_path} no existe ")
except PermissionError:
    print(f"Permiso denegado para abrir el archivo {file_path} ")
except IOError as e:
    print(f"Error al abrir el archivo {file_path} : {e} ")
except Exception as e:
    print(f"Error general : {e}")

## try for database
try:
    db_connection = sqlite3.connect('data.db')
except sqlite3.Error as e:
    print(f"Error al abrir la base {e} ")
except Exception as e:
    print(f"Error general al abrir la base de datos {e} ")
finally:
    if db_connection:
        db_connection.close()
## try for invoke service

try:
    requests.get('https://api/alumnos/service')
except requests.exceptions.ConnectionError:
    print('Error no se puede conectar al servidor')
except requests.exceptions.Timeout:
    print("El servicios no responde")
except requests.exceptions.RequestException as e:
    print(f"Error al invocar al servicios {e} ")
except Exception as e:
    print(f"Error general al abrir la base de datos {e} ")

























