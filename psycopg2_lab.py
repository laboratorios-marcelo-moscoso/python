import psycopg2

# Establish a connection to the PostgreSQL database
connection = psycopg2.connect(
    host="host_name",
    port="port_number",
    database="database_name",
    user="username",
    password="password"
)

# Create a cursor object to interact with the database
cursor = connection.cursor()

# Execute a SQL query
cursor.execute("SELECT * FROM employees")

# Fetch and print the result set
for row in cursor:
    print(row)

# Close the cursor and the connection
cursor.close()
connection.close()
