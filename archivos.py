# Leer desde un archivo
file_path = "datos_3.txt"
file_path_2 = "datos_2.txt"

print('Lee el contenido del archivo')
with open(file_path, "r") as file:
    content = file.read()
    print("Contenido del archivo:\n", content)

print('lee linea a linea')
with open(file_path,"r") as file:
    for line in file:
        print(line)

# Escribir en un archivo (sobrescribir contenido existente)
with open(file_path_2, "w") as file:
    file.write("¡Hola, Mundo ! \n")
    file.write("python modulos \n")
    file.write("python funciones \n")
    file.write("python operaciones \n")


# Agregar contenido a un archivo (log)
with open(file_path, "a") as file:
    file.write("\n Agregando texto adicional.")


