import PySimpleGUI as sg
import random

sg.theme('BlueMono')

def get_aleatorio():
    return random.randint(1,8)

# disenio de la pantalla
layout = [
    [sg.Text('Sorteo de frutas',font=('Verdana',20))],
    [sg.Image(r'frutas/cereza.png'),sg.Image(r'frutas/pinia.png'),sg.Image(r'frutas/uva.png')],
    [sg.Button('Jugar',font=('Verdana',20))]
]

window = sg.Window('SORTEO DE FRUTAS',layout)

while True:
    event,value = window.read()
    if event == sg.WIN_CLOSED:
        print('estamos cerrando la ventana')
        break
    if event == 'Jugar':
        print(get_aleatorio())
        print(get_aleatorio())
        print(get_aleatorio())

window.close()