import json

file_path = "alumnos.json"

with open(file_path, "r") as file:
    alumnos = json.load(file)


for alumno in alumnos:
    nombre = alumno["nombre_completo"]
    email = alumno["email"]
    print(f"Nombre: {nombre}, email: {email} ")