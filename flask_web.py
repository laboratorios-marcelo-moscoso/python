from flask import Flask

app = Flask(__name__)

#definimos las rutas
@app.route('/')
def hola():
    return 'Hola mundo Flask'

@app.route('/alumnos')
def alumnos():
    return 'welcome alumnos'

if __name__ == '__main__':
    app.run()
