import pymssql
from mi_paquete import server,user,password,database


# Establish a connection to the SQL Server database
connection = pymssql.connect(
    server=server,
    user=user,
    password=password,
    database=database
)

# Create a cursor object to interact with the database
cursor = connection.cursor()

# Execute a SQL query
cursor.execute('SELECT Pmt_Tipo,Pmt_Parametro FROM Seg_Parametros ')

# Fetch and print the result set
for row in cursor:
    print(row)

# Close the cursor and the connection
cursor.close()
connection.close()
