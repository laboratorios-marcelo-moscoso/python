
# creando un diccionario
alumno = {
    "nombre" : "Ana Gomez",
    "edad" : 32,
    "email" : "ana@test.com"
}

# accediendo al valor de un diccionario
print(alumno["nombre"])

# modificando un valor al diccionario
alumno["email"] = "ana.gomez@test.com"

## del alumno["edad"]

if "email" in alumno:
    print('email se encuentra en el diccionario')

## obtener las keys de un diccionario
for key in alumno:
    print(key,":", alumno[key])

## metodos

## alumno.clear()

alumno_copia = alumno.copy()

email = alumno.get("email")

for key,value in alumno.items():
    print(key,":", value)

keys = alumno.keys()
print(keys) 

values = alumno.values()
print(values)

## remover
##alumno.pop("email")

alumno_info = {
    "doc_identidad" : 123456,
    "contacto" : "372347"
}

alumno.update(alumno_info)
print(alumno)


















