
# 1. forma: 
# from operaciones import suma,multiplica
# result = suma(123,222)

# 2. forma:
# import operaciones
# result = operaciones.suma(123,222)

# 3. forma:
import operaciones as op 
import random
from mi_paquete import getAlumnos,getCursos

def lanzar_dado():
    return random.randint(1,6)

result = op.suma(123,345)

print(result)

print(lanzar_dado())

print(getAlumnos())
print(getCursos())


