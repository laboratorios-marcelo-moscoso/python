1. math.sqrt(x): Devuelve la raíz cuadrada de un número.
2. math.ceil(x): Devuelve el entero más pequeño mayor o igual que un número dado.
3. math.floor(x): Devuelve el entero más grande menor o igual que un número dado.
4. math.pow(x, y): Devuelve x elevado a la potencia y.
5. math.sin(x): Devuelve el seno de un ángulo dado en radianes.
6. math.cos(x): Devuelve el coseno de un ángulo dado en radianes.
7. math.tan(x): Devuelve la tangente de un ángulo dado en radianes.
8. math.log(x): Devuelve el logaritmo natural (base e) de un número dado.
9. math.exp(x): Devuelve e elevado a la potencia x.
10. math.radians(grados): Convierte un ángulo de grados a radianes.
 Agregando texto adicional.

 https://codigoinfo.xyz/api/alumnos