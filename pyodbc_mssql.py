import pyodbc

# Establish a connection to the SQL Server database
connection = pyodbc.connect(
    "Driver={SQL Server};"
    "Server=server_name;"
    "Database=database_name;"
    "UID=username;"
    "PWD=password;"
)

# Create a cursor object to interact with the database
cursor = connection.cursor()

# Execute a SQL query
cursor.execute("SELECT * FROM employees")

# Fetch and print the result set
for row in cursor:
    print(row)

# Close the cursor and the connection
cursor.close()
connection.close()
