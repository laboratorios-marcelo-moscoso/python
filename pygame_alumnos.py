import pygame.camera
import pygame.image

# Initialize Pygame and the camera
pygame.init()
pygame.camera.init()

# Get the list of available cameras
cam_list = pygame.camera.list_cameras()

# Check if any camera is available
if not cam_list:
    print("No camera found")
    exit()

# Create a camera object
cam = pygame.camera.Camera(cam_list[0])

# Start the camera
cam.start()

# Create a Pygame display window
screen = pygame.display.set_mode((640, 480))

# Main loop to capture and display frames
running = True
while running:
    # Capture a frame from the camera
    image = cam.get_image()

    # Display the frame on the Pygame window
    screen.blit(image, (0, 0))
    pygame.display.flip()

    # Check for key press to exit
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

# Stop the camera and quit Pygame
cam.stop()
pygame.quit()
