from flask import Flask,jsonify,request
from flask_cors import CORS
from cursos_json import cursos


app = Flask(__name__)
CORS(app)

#routes
@app.route('/api-alumnos/',methods=['GET'])
def index():
    return '<h1>api rest flask </h1 '

@app.route('/api-alumno/cursos',methods=['GET'])
def get_cursos():
    return jsonify(cursos)

@app.route('/api-alumno/cursos/<id>',methods=['GET'])
def get_curso(id):
    responde=[curso for curso in cursos if curso['ID'] == int(id)]
    if(len(responde)>0):
        return jsonify(responde)
    return jsonify({"mensaje":"curso no encontrado {}".format(id) })

@app.route('/api-alumno/cursos',methods=['POST'])
def add_curso():
    nuevo_curso = {
        "ID":len(cursos)+1,
        "nombre": request.json['nombre'],
        "categoria": request.json['categoria'],
    }
    cursos.append(nuevo_curso)
    return jsonify({"mensaje":"curso adicionado"})


@app.route('/api-alumno/cursos/<id>',methods=['PUT'])
def actulizar_curso(id):
    responde=[curso for curso in cursos if curso['ID'] == int(id)]
    if(len(responde)>0):
        responde[0]['nombre'] = request.json['nombre']
        responde[0]['categoria'] = request.json['categoria']
        return jsonify({"mensaje":"curso actualizado"})
    return jsonify({"mensaje":"curso no encontrado"})

@app.route('/api-alumno/cursos/<id>',methods=['DELETE'])
def delete_curso(id):
    responde=[curso for curso in cursos if curso['ID'] == int(id)]
    if(len(responde)>0):
        cursos.remove(responde[0])
        return jsonify({"mensaje":"curso eliminado"})
    return jsonify({"mensaje":"curso no encontrado"})

# server
if __name__ == '__main__':
    app.run(debug=True,port=3000)
