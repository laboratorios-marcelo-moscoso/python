import cx_Oracle

# Establish a connection to the Oracle database
connection = cx_Oracle.connect("username", "password", "host:port/service_name")

# Create a cursor object to interact with the database
cursor = connection.cursor()

# Execute a SQL query
cursor.execute("SELECT * FROM employees")

# Fetch and print the result set
for row in cursor:
    print(row)

# Close the cursor and the connection
cursor.close()
connection.close()
