import re


text = "2023-06-19T00:34:55 python y el modulo RegEx, es una secuencia de caracteres que forma un patron de busquedad "


pattern = r'2023-06-19'
match = re.search(pattern, text)

if match:
    print(match.group())
else:
    print('No Match')