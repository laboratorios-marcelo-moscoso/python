import sqlite3
import pyodbc

# SQLite connection parameters
sqlite_db_file = 'segurosdb.sqlite3'  # Replace with the path to your SQLite database file
sqlite_table_name = 'deudors'  # Replace with your SQLite table name

# SQL Server connection parameters
server = 'DESKTOP-IEG83EI\SQLEXPRESS'  # Replace with your SQL Server name or IP address
database = 'colibri2020_dh_ext'  # Replace with your SQL Server database name
username = 'sa'  # Replace with your SQL Server username
password = 'desa2023'  # Replace with your SQL Server password
sql_server_table_name = 'deudors'  # Replace with your SQL Server table name

message = ''
offset = 0
total = 22082

def createSaveLog(line):
    file_path = f'log_{sqlite_table_name}.log'
    try:
        # Open the file in append mode ('a')
        with open(file_path, 'a') as file:
            # Write data to the file
            file.write(line)
            file.write("\n")
    except IOError:
        print(f"Error writing to file '{file_path}'.")

def process():
    try:
        # Connect to SQLite database
        sqlite_conn = sqlite3.connect(sqlite_db_file)
        sqlite_cursor = sqlite_conn.cursor()

        # Select data from SQLite table
        sqlite_cursor.execute(f'SELECT * FROM {sqlite_table_name} order by id limit {total} OFFSET {offset} ')
        rows = sqlite_cursor.fetchall()

        # Connect to SQL Server database
        sql_server_conn_str = f'DRIVER={{SQL Server}};SERVER={server};DATABASE={database};UID={username};PWD={password}'
        sql_server_conn = pyodbc.connect(sql_server_conn_str)
        sql_server_cursor = sql_server_conn.cursor()

        # Insert data into SQL Server table
        sql_server_cursor.execute(f'SET IDENTITY_INSERT {sql_server_table_name} ON')
        for row in rows:
            message = f'ERROR {offset} : {row}'
            sql_query = f"INSERT INTO {sql_server_table_name}  (id, id_folder, id_prestamo, nombre, apellido_paterno, apellido_materno, apellido_casada, direccion_domicilio, direccion_negocio, estado_civil, sexo, numero_documento, doc_identidad_expedido, tipo_documento, complemento_documento, pais_nacimiento, lugar_nacimiento, fecha_nacimiento, peso, talla, manejo_mano, actividad, detalle_actividad, fecha_registro, celular, telefono_contacto, telefono, edad, seguro_anterior, numero_solicitud_anterior, saldo_actual_verificado, saldo_actual_solicitud, lugar_trabajo, ocupacion, nit_factura, nombre_usuario, pais, caedec, id_cliente, id_caedec, telefono_1, telefono_2, correo_electronico, profesion, origen, nro_certificado, saldo, aprobado, rechazado, extra_prima, condiciones_exclusiones, createdAt, updatedAt, cargo, fecha_ingreso, actividad_economica, direccion_comercial, zona, pais_residencia, nro_certificado_2, correlativo, nro_certificado_rep) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  "
            sql_server_cursor.execute(sql_query, row)
            sql_server_conn.commit()
        sql_server_cursor.execute(f'SET IDENTITY_INSERT {sql_server_table_name} OFF')

        # Close the cursors and connections
        sqlite_cursor.close()
        sqlite_conn.close()
        
        sql_server_cursor.close()
        sql_server_conn.close()

    except sqlite3.Error as e:
        print(f'Error connecting to SQLite database: {e}')

    except pyodbc.Error as e:
        print(f'Error connecting to SQL Server: {e}')
        print(message)
        createSaveLog(message)

"""
while offset  <= total:
    process()
    offset+=1
"""

#
process()