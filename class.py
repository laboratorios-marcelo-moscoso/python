
class Alumno:
    def __init__(self, nombre, edad, nota):
        self.nombre = nombre
        self.edad = edad
        self.nota = nota
    
    def obtener_informacion(self):
        return f"Nombre: {self.nombre}, Edad: {self.edad}, Nota: {self.nota}"
    
    def obtener_estado(self):
        if self.nota >= 60:
            return "Aprobado"
        else:
            return "Reprobado"

# Crear instancias de Alumno
alumno1 = Alumno("Juan", 20, 75)
alumno2 = Alumno("María", 22, 55)

# Obtener información de los alumnos
print(alumno1.obtener_informacion())  # Output: Nombre: Juan, Edad: 20, Nota: 75
print(alumno2.obtener_informacion())  # Output: Nombre: María, Edad: 22, Nota: 55

# Obtener estado de los alumnos
print(alumno1.obtener_estado())  # Output: Aprobado
print(alumno2.obtener_estado())  # Output: Reprobado

## herencia
class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
    
    def saludar(self):
        print(f"Hola, mi nombre es {self.nombre} y tengo {self.edad} años.")

class Estudiante(Persona):
    def __init__(self, nombre, edad, carrera):
        super().__init__(nombre, edad)
        self.carrera = carrera
    
    def estudiar(self):
        print(f"Estoy estudiando {self.carrera}.")

# Crear una instancia de Estudiante
estudiante1 = Estudiante("Juan", 20, "Ingeniería")

# Acceder a los atributos de Persona
print(estudiante1.nombre)  # Output: Juan
print(estudiante1.edad)  # Output: 20

# Llamar a los métodos de Persona
estudiante1.saludar()  # Output: Hola, mi nombre es Juan y tengo 20 años.

# Acceder al atributo propio de Estudiante
print(estudiante1.carrera)  # Output: Ingeniería

# Llamar al método propio de Estudiante
estudiante1.estudiar()  # Output: Estoy estudiando Ingeniería.

## herencia multiple

class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
    
    def saludar(self):
        print(f"Hola, mi nombre es {self.nombre} y tengo {self.edad} años.")

class Estudiante(Persona):
    def __init__(self, nombre, edad, carrera):
        super().__init__(nombre, edad)
        self.carrera = carrera
    
    def estudiar(self):
        print(f"Estoy estudiando {self.carrera}.")

class Empleado:
    def __init__(self, nombre, salario):
        self.nombre = nombre
        self.salario = salario
    
    def trabajar(self):
        print(f"{self.nombre} está trabajando.")

class EstudianteEmpleado(Estudiante, Empleado):
    def __init__(self, nombre, edad, carrera, salario):
        Estudiante.__init__(self, nombre, edad, carrera)
        Empleado.__init__(self, nombre, salario)

# Crear una instancia de EstudianteEmpleado
estudiante_empleado = EstudianteEmpleado("Juan", 20, "Ingeniería", 5000)

# Acceder a los atributos y métodos heredados de Persona
print(estudiante_empleado.nombre)  # Output: Juan
print(estudiante_empleado.edad)  # Output: 20
estudiante_empleado.saludar()  # Output: Hola, mi nombre es Juan y tengo 20 años.

# Acceder a los atributos y métodos heredados de Estudiante
print(estudiante_empleado.carrera)  # Output: Ingeniería
estudiante_empleado.estudiar()  # Output: Estoy estudiando Ingeniería.

# Acceder a los atributos y métodos heredados de Empleado
print(estudiante_empleado.salario)  # Output: 5000
estudiante_empleado.trabajar()  # Output: Juan está trabajando.

## Polimorfismo

class Empleado:
    def __init__(self, nombre, salario):
        self.nombre = nombre
        self.salario = salario
    
    def trabajar(self):
        print(f"{self.nombre} está trabajando.")
    
    def calcular_salario(self):
        print(f"El salario de {self.nombre} es: {self.salario}")


class Gerente(Empleado):
    def __init__(self, nombre, salario, bono):
        super().__init__(nombre, salario)
        self.bono = bono
    
    def trabajar(self):
        print(f"{self.nombre} está gestionando el equipo.")

    def calcular_salario(self):
        total_salario = self.salario + self.bono
        print(f"El salario de {self.nombre} (con bono) es: {total_salario}")


# Crear instancias de las clases
empleado1 = Empleado("Juan", 5000)
gerente1 = Gerente("María", 7000, 2000)

# Polimorfismo en acción
empleados = [empleado1, gerente1]

for empleado in empleados:
    empleado.trabajar()
    empleado.calcular_salario()
    print("-------------")

## Encapsulacion

class Estudiante:
    def __init__(self, nombre, edad):
        self.__nombre = nombre
        self.__edad = edad
    
    def get_nombre(self):
        return self.__nombre
    
    def set_nombre(self, nombre):
        self.__nombre = nombre
    
    def get_edad(self):
        return self.__edad
    
    def set_edad(self, edad):
        self.__edad = edad


# Crear una instancia de la clase Estudiante
estudiante1 = Estudiante("Juan", 20)

# Acceder a los atributos directamente (sin encapsulación)
print(estudiante1.__nombre)  # Genera un error

# Acceder a los atributos utilizando los métodos getter
print(estudiante1.get_nombre())  # Output: Juan
print(estudiante1.get_edad())  # Output: 20

# Modificar los atributos utilizando los métodos setter
estudiante1.set_nombre("María")
estudiante1.set_edad(22)

print(estudiante1.get_nombre())  # Output: María
print(estudiante1.get_edad())  # Output: 22


## metodos static

class OperacionesUtil:

    def add(a,b):
        return a + b

    def multiply(a,b):
        return a * b
    
    def __ope(self):
        return 'metodo es privado'
    

resultado = OperacionesUtil.add(3,7)
print('resultado de la suma es: ',resultado)

OperacionesUtil.__ope()

