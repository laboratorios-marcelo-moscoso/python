import PySimpleGUI as sg
import requests
import json

url_api = 'https://codigoinfo.xyz/api/alumnos'

table_data = []

layout = [
    [sg.Combo(['GET', 'POST','PUT','DELETE'], key='-COMBO-', enable_events=True,font=('Verdana',10) ), 
     sg.InputText(key='-URL-',default_text=url_api,font=('Verdana',10)) ],
    [sg.Button('Invoke API',font=('Verdana',10), key='invoke_btn')],
    [sg.Table(values=table_data,
              headings=['ID','Nombre completo','Email'],
              key='-TABLE-',
              font=('Verdana',10),
              auto_size_columns=True,
              expand_x=True,
              expand_y=True,
              )],
    [sg.Multiline(key='-BODY-',font=('verdana',10) ,size=(60, 10))],
]


window = sg.Window('REST API Invocation', layout)


while True:
    event, values = window.read()

    if event == sg.WINDOW_CLOSED:
        break

    if event == 'invoke_btn':
        url = values['-URL-']
        combo = values['-COMBO-']

        if combo == 'GET':
            response = requests.get(url)
            data = response.json()
            print(data['data'])
            for row in data['data']:
                table_data.append([row['_id'],row['nombre_completo'],row['email']])

            window['-BODY-'].update(response.text)
            window['-TABLE-'].update(values=table_data)

        if combo == 'POST':
            body = values['-BODY-']
            print(body)
            json_obj = json.loads(body)
            json_data = json.dumps(json_obj)
            print(json_data)
            headers = {'Content-Type': 'application/json'}
            response = requests.post(url, data=json_data,headers=headers)
            window['-BODY-'].update(response.text)
    
window.close()
